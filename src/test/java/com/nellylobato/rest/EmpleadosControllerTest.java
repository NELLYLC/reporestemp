package com.nellylobato.rest;

import com.nellylobato.rest.utils.BadSeparator;
import com.nellylobato.rest.utils.EstadosPedido;
import com.nellylobato.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
//import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.*;


@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }

    @Test
    public void testSeparadorGetCadena(){
        try {
            Utilidades.getCadena("Nelly Lobato", "..");
            fail("Se esperaba BadSeparador");
        }catch (BadSeparator bs){

        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 15, -3, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEsBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    //@EmptySource
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void testEsBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }

   /*@Test
    public void testGetAutor(){
        assertEquals("Nelly Lobato", empleadosController.getAppAutor());
    }*/
}
